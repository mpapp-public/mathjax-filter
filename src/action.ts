/*!
 * © 2020 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LiteDocument } from 'mathjax-full/js/adaptors/lite/Document'
import { LiteAdaptor } from 'mathjax-full/js/adaptors/liteAdaptor'
import { HTMLDocument } from 'mathjax-full/js/handlers/html/HTMLDocument'
import { TeX } from 'mathjax-full/js/input/tex'
import { AllPackages } from 'mathjax-full/js/input/tex/AllPackages'
import { SVG } from 'mathjax-full/js/output/svg'
import { metaMapToRaw, RawInline, SingleFilterActionAsync } from 'pandoc-filter'

const adaptor = new LiteAdaptor()

const doc = new HTMLDocument(new LiteDocument(), adaptor, {
  // TeX input
  InputJax: new TeX({
    packages: AllPackages,
  }),

  // SVG output
  OutputJax: new SVG({
    fontCache: 'none', // avoid <defs> and <use xlink:href>
  }),
})

const convertTeXToSVG = (
  tex: string,
  display: boolean,
  meta?: Record<string, unknown>
): string => {
  const svg = doc.convert(tex, {
    display,
    // em: 16,
    // ex: 8,
    // containerWidth: 1000000,
    // lineWidth: 1000000,
    // scale: 1,
    // linebreaks: false,
    ...meta,
  })

  return adaptor.innerHTML(svg)
}

export const action: SingleFilterActionAsync = async (ele, format, meta) => {
  if (ele.t === 'Math') {
    const [mathType, tex] = ele.c

    const display = mathType.t === 'DisplayMath'

    const options = metaMapToRaw(meta) as {
      'mathjax.typeset': Record<string, unknown> | undefined
      'mathjax.centerDisplayMath': boolean | undefined
    }

    const svg = convertTeXToSVG(tex, display, options['mathjax.typeset'])

    if (!display) {
      return RawInline('html', svg)
    }

    return [
      RawInline(
        'html',
        options['mathjax.centerDisplayMath']
          ? '<p style="text-align: center">'
          : '<p>'
      ),
      RawInline('html', svg),
      RawInline('html', '</p>'),
    ]
  }
}
